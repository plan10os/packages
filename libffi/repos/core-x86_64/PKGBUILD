# Maintainer: Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor: Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor: Stéphane Gaudreault <stephane@archlinux.org>
# Contributor: Jan de Groot <jgc@archlinux.org>

pkgname=libffi
pkgver=3.4.3
pkgrel=1
pkgdesc='Portable foreign function interface library'
arch=('x86_64')
url='https://sourceware.org/libffi/'
license=('MIT')
depends=('glibc')
checkdepends=('dejagnu')
provides=('libffi.so')
options=('debug')
source=(https://github.com/libffi/libffi/releases/download/v$pkgver/libffi-$pkgver.tar.gz)
sha256sums=('4416dd92b6ae8fcb5b10421e711c4d3cb31203d77521a77d85d0102311e6c3b8')
b2sums=('5e751c53a6b65316e438723810fbafe7f27732feb50466f1459d086c35a519f460b57968721212496a7502b0a5860546c84b22ec269e979728f18d0731fc918a')

build() {
  cd $pkgname-$pkgver
  # remove --disable-exec-static-tramp once ghc and gobject-introspection
  # work fine with it enabled (https://github.com/libffi/libffi/pull/647)
  ./configure \
    --prefix=/usr \
    --disable-static \
    --disable-multi-os-directory \
    --disable-exec-static-tramp \
    --enable-pax_emutramp
  make
}

check() {
  make -C $pkgname-$pkgver check
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm 644 LICENSE -t "$pkgdir"/usr/share/licenses/$pkgname
  install -Dm 644 README.md -t "$pkgdir"/usr/share/doc/$pkgname
}

# vim:set ts=2 sw=2 et:

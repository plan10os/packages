# Maintainer: Antonio Rojas <arojas@archlinux,org>

pkgname=krita
_pkgver=5.1.1
pkgver=${_pkgver/-/}
pkgrel=2
pkgdesc='Edit and paint images'
arch=(x86_64)
url='https://krita.org'
license=(GPL3)
depends=(kitemviews kitemmodels ki18n kcompletion kguiaddons kcrash qt5-svg qt5-multimedia quazip
         gsl libraw exiv2 openexr fftw openjpeg2 opencolorio libwebp hicolor-icon-theme)
makedepends=(extra-cmake-modules kdoctools boost eigen poppler-qt5 python-pyqt5 libheif
             qt5-tools sip kseexpr libmypaint libjxl xsimd)
optdepends=('poppler-qt5: PDF filter' 'ffmpeg: to save animations'
            'python-pyqt5: for the Python plugins' 'libheif: HEIF filter'
            'kseexpr: SeExpr generator layer' 'kimageformats: PSD support' 'libmypaint: support for MyPaint brushes'
            'krita-plugin-gmic: GMic plugin' 'libjxl: JPEG-XL filter')
source=(https://download.kde.org/stable/krita/$_pkgver/$pkgname-$_pkgver.tar.gz{,.sig}
        krita-xsimd-9.patch)
sha256sums=('4ddbf897afa7b187131697d12a23ba39894638068b2eae12b3b434fe42ad8e24'
            'SKIP'
            '84767e7da9ad861b5a95550f3cd06f45768f8f39eb4a9ce8cdb674163e763234')
validpgpkeys=('05D00A8B73A686789E0A156858B9596C722EA3BD'  # Boudewijn Rempt <foundation@krita.org>
              'E9FB29E74ADEACC5E3035B8AB69EB4CF7468332F'  # Dmitry Kazakov (main key) <dimula73@gmail.com>
              '064182440C674D9F8D0F6F8B4DA79EDA231C852B') # Stichting Krita Foundation <foundation@krita.org>
options=(debug)

prepare() {
  patch -d $pkgname-$pkgver -p1 < krita-xsimd-9.patch # Support xsimd 9
}

build() {
  cmake -B build -S $pkgname-$_pkgver \
    -DBUILD_KRITA_QT_DESIGNER_PLUGINS=ON
  cmake --build build
}

package() {
  DESTDIR="$pkgdir" cmake --install build
}

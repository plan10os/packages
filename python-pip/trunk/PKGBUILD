# Maintainer: David Runge <dvzrv@archlinux.org>
# Maintainer: Felix Yan <felixonmars@archlinux.org>
# Contributor: Dan McGee <dan@archlinux.org>
# Contributor: Eli Schwartz <eschwartz@archlinux.org>
# Contributor: Sebastien Binet <binet@lblbox>

_name=pip
pkgname=python-pip
pkgver=22.3
pkgrel=1
pkgdesc="The PyPA recommended tool for installing Python packages"
url="https://pip.pypa.io/"
arch=(any)
license=(MIT)
depends=(
  python
  python-attrs  # for vendored rich
  python-cryptography  # for vendored requests and urllib3
  python-wheel  # for internal misc utils
)
makedepends=(
  python-build
  python-installer
  python-myst-parser
  python-setuptools
  python-sphinx
  python-sphinx-copybutton
  python-sphinx-inline-tabs
  python-sphinxcontrib-towncrier
)
checkdepends=(
  git
  python-freezegun
  python-pytest
  python-pytest-rerunfailures
  python-pytest-xdist
  python-scripttest
  python-tomli-w
  python-virtualenv
  python-werkzeug
  subversion
)
source=($_name-$pkgver.tar.gz::https://github.com/pypa/$_name/archive/$pkgver.tar.gz)
sha512sums=('804c5bad805da77d922e5e123440f6b7639a33769b41ab5cd1c27e5c5f0e4548ce6e5a0cc4c4d4fa42d25e4699d67d60db059045f97ba974b2ea4d552683a693')
b2sums=('68c0151f9c1ae14a6cb9b41d39521b3898b156f642361df5f3b272e4ba1640e794189d5d991dcb56156745c587cfbe02161763683965c2bb8a69adb9f109fd00')

build() {
  cd $_name-$pkgver

  python -m build --wheel --no-isolation

  cd docs/
  PYTHONPATH="$srcdir/$_name-$pkgver/src/" python pip_sphinxext.py
  PYTHONPATH="$srcdir/$_name-$pkgver/src/" sphinx-build -b man -d build/doctrees/man man build/man -c html
}

check() {
  local _site_packages=$(python -c "import site; print(site.getsitepackages()[0])")
  local _deselected=(
    # deselect tests that would require python-pip in checkdepends (may or may not work due to devendored dependencies when python-pip is updated)
    --deselect tests/unit/test_direct_url_helpers.py::test_from_link_vcs_with_source_dir_obtains_commit_id
    --deselect tests/unit/test_direct_url_helpers.py::test_from_link_vcs_without_source_dir
    --deselect tests/unit/test_wheel_builder.py::test_should_cache_git_sha
  )

  cd $_name-$pkgver
  # install to temporary location
  python -m installer --destdir=test_dir dist/*.whl
  export PYTHONPATH="test_dir/$_site_packages:$PYTHONPATH"
  pytest -vv -m unit "${_deselected[@]}"
}

package() {
  local _site_packages=$(python -c "import site; print(site.getsitepackages()[0])")

  cd $_name-$pkgver
  python -m installer --destdir="$pkgdir" dist/*.whl

  install -vDm 644 LICENSE.txt -t "$pkgdir/usr/share/licenses/$pkgname/"
  install -vDm 644 docs/build/man/*.1 -t "$pkgdir/usr/share/man/man1/"
  install -vDm 644 {NEWS,README}.rst -t "$pkgdir/usr/share/doc/$pkgname/"

  # NOTE: zsh completion does not work: https://bugs.archlinux.org/task/65349#comment187166
  PYTHONPATH="$pkgdir/$_site_packages" "$pkgdir"/usr/bin/pip completion --bash | install -vDm 644 /dev/stdin "$pkgdir"/usr/share/bash-completion/completions/pip
  PYTHONPATH="$pkgdir/$_site_packages" "$pkgdir"/usr/bin/pip completion --fish | install -vDm 644 /dev/stdin "$pkgdir"/usr/share/fish/vendor_completions.d/pip.fish
}

# Maintainer: Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor: Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor: Ionut Biru <ibiru@archlinux.org>
# Contributor: Alexander Baldeck <alexander@archlinux.org>
# Contributor: Dale Blount <dale@archlinux.org>
# Contributor: Anders Bostrom <anders.bostrom@home.se>

pkgbase=thunderbird
pkgname=(thunderbird)
pkgver=102.4.0
pkgrel=2
pkgdesc='Standalone mail and news reader from mozilla.org'
url='https://www.mozilla.org/thunderbird/'
arch=(x86_64)
license=(MPL GPL LGPL)
depends=(
  glibc gtk3 libgdk-3.so libgtk-3.so mime-types dbus libdbus-1.so dbus-glib
  alsa-lib nss hunspell sqlite ttf-font libvpx libvpx.so zlib bzip2 libbz2.so
  botan libwebp libwebp.so libwebpdemux.so libevent libjpeg-turbo libffi
  libffi.so nspr gcc-libs libx11 libxrender libxfixes libxext libxcomposite
  libxdamage pango libpango-1.0.so cairo gdk-pixbuf2 icu libicui18n.so
  libicuuc.so freetype2 libfreetype.so fontconfig libfontconfig.so glib2
  libglib-2.0.so pixman libpixman-1.so gnupg
)
makedepends=(
  unzip zip diffutils python python-setuptools yasm nasm mesa imake libpulse
  xorg-server-xvfb autoconf2.13 rust clang llvm cbindgen nodejs lld
  gawk perl findutils libotr wasi-compiler-rt wasi-libc wasi-libc++ wasi-libc++abi
)
options=(!emptydirs !makeflags !lto)
source=(https://ftp.mozilla.org/pub/mozilla.org/thunderbird/releases/$pkgver/source/thunderbird-$pkgver.source.tar.xz{,.asc}
        thunderbird.desktop
        vendor-prefs.js
        distribution.ini
        mozconfig.cfg
        metainfo.patch
        cbindgen-0.24.patch
        rustc_version-0.4.0.patch)
validpgpkeys=(
  14F26682D0916CDD81E37B6D61B7B526D98F0353 # Mozilla Software Releases <release@mozilla.com>
  4360FE2109C49763186F8E21EBE41E90F6F12F6D # Mozilla Software Releases <release@mozilla.com>
)

# Google API keys (see http://www.chromium.org/developers/how-tos/api-keys)
# Note: These are for Arch Linux use ONLY. For your own distribution, please
# get your own set of keys. Feel free to contact foutrelis@archlinux.org for
# more information.
_google_api_key=AIzaSyDwr302FpOSkGRpLlUpPThNTDPbXcIn_FM

# Mozilla API keys (see https://location.services.mozilla.com/api)
# Note: These are for Arch Linux use ONLY. For your own distribution, please
# get your own set of keys. Feel free to contact heftig@archlinux.org for
# more information.
_mozilla_api_key=16674381-f021-49de-8622-3021c5942aff

prepare() {
  cd $pkgname-$pkgver

  echo "${noextract[@]}"

  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    [[ $src = *.patch ]] || continue
    msg2 "Applying patch $src..."
    patch -Np1 < "../$src"
  done

  printf "%s" "$_google_api_key" >google-api-key
  printf "%s" "$_mozilla_api_key" >mozilla-api-key
  cp ../mozconfig.cfg .mozconfig
  sed "s|@PWD@|${PWD@Q}|g" -i .mozconfig
}

build() {
  cd $pkgname-$pkgver
  if [[ -n "${SOURCE_DATE_EPOCH}" ]]; then
    export MOZ_BUILD_DATE=$(date --date "@${SOURCE_DATE_EPOCH}" "+%Y%m%d%H%M%S")
  fi
  export MACH_BUILD_PYTHON_NATIVE_PACKAGE_SOURCE=none
  export MOZBUILD_STATE_PATH="${srcdir}/mozbuild"
  ./mach configure
  ./mach build
  ./mach buildsymbols
}

package_thunderbird() {
  optdepends=(
    'libotr: OTR support for active one-to-one chats'
    'libnotify: Notification integration'
  )

  cd $pkgname-$pkgver
  DESTDIR="$pkgdir" ./mach install

  install -Dm 644 ../vendor-prefs.js -t "$pkgdir/usr/lib/$pkgname/defaults/pref"
  install -Dm 644 ../distribution.ini -t "$pkgdir/usr/lib/$pkgname/distribution"
  install -Dm 644 ../thunderbird.desktop -t "$pkgdir/usr/share/applications"
  install -Dm 644 comm/mail/branding/thunderbird/net.thunderbird.Thunderbird.appdata.xml \
    "$pkgdir/usr/share/metainfo/net.thunderbird.Thunderbird.appdata.xml"

  for i in 16 22 24 32 48 64 128 256; do
    install -Dm644 comm/mail/branding/thunderbird/default${i}.png \
      "$pkgdir/usr/share/icons/hicolor/${i}x${i}/apps/$pkgname.png"
  done
  install -Dm644 comm/mail/branding/thunderbird/TB-symbolic.svg \
    "$pkgdir/usr/share/icons/hicolor/symbolic/apps/thunderbird-symbolic.svg"

  # Use system-provided dictionaries
  ln -Ts /usr/share/hunspell "$pkgdir/usr/lib/$pkgname/dictionaries"
  ln -Ts /usr/share/hyphen "$pkgdir/usr/lib/$pkgname/hyphenation"

  # Install a wrapper to avoid confusion about binary path
  install -Dm755 /dev/stdin "$pkgdir/usr/bin/$pkgname" <<END
#!/bin/sh
exec /usr/lib/$pkgname/thunderbird "\$@"
END

  # Replace duplicate binary with wrapper
  # https://bugzilla.mozilla.org/show_bug.cgi?id=658850
  ln -srf "$pkgdir/usr/bin/$pkgname" \
    "$pkgdir/usr/lib/$pkgname/thunderbird-bin"
}

_package_i18n() {
  pkgdesc="$2 language pack for Thunderbird"
  depends=("thunderbird>=$pkgver")
  install -Dm644 thunderbird-i18n-$pkgver-$1.xpi \
    "$pkgdir/usr/lib/thunderbird/extensions/langpack-$1@thunderbird.mozilla.org.xpi"
}

_languages=(
  'af     "Afrikaans"'
  'ar     "Arabic"'
  'ast    "Asturian"'
  'be     "Belarusian"'
  'bg     "Bulgarian"'
  'br     "Breton"'
  'ca     "Catalan"'
  'cak    "Kaqchikel"'
  'cs     "Czech"'
  'cy     "Welsh"'
  'da     "Danish"'
  'de     "German"'
  'dsb    "Lower Sorbian"'
  'el     "Greek"'
  'en-GB  "English (British)"'
  'en-US  "English (US)"'
  'es-AR  "Spanish (Argentina)"'
  'es-ES  "Spanish (Spain)"'
  'et     "Estonian"'
  'eu     "Basque"'
  'fi     "Finnish"'
  'fr     "French"'
  'fy-NL  "Frisian"'
  'ga-IE  "Irish"'
  'gd     "Gaelic (Scotland)"'
  'gl     "Galician"'
  'he     "Hebrew"'
  'hr     "Croatian"'
  'hsb    "Upper Sorbian"'
  'hu     "Hungarian"'
  'hy-AM  "Armenian"'
  'id     "Indonesian"'
  'is     "Icelandic"'
  'it     "Italian"'
  'ja     "Japanese"'
  'ka     "Georgian"'
  'kab    "Kabyle"'
  'kk     "Kazakh"'
  'ko     "Korean"'
  'lt     "Lithuanian"'
  'ms     "Malay"'
  'nb-NO  "Norwegian (Bokmål)"'
  'nl     "Dutch"'
  'nn-NO  "Norwegian (Nynorsk)"'
  'pa-IN  "Punjabi (India)"'
  'pl     "Polish"'
  'pt-BR  "Portuguese (Brazilian)"'
  'pt-PT  "Portuguese (Portugal)"'
  'rm     "Romansh"'
  'ro     "Romanian"'
  'ru     "Russian"'
  'sk     "Slovak"'
  'sl     "Slovenian"'
  'sq     "Albanian"'
  'sr     "Serbian"'
  'sv-SE  "Swedish"'
  'th     "Thai"'
  'tr     "Turkish"'
  'uk     "Ukrainian"'
  'uz     "Uzbek"'
  'vi     "Vietnamese"'
  'zh-CN  "Chinese (Simplified)"'
  'zh-TW  "Chinese (Traditional)"'
)
_url=https://ftp.mozilla.org/pub/mozilla.org/thunderbird/releases/$pkgver/linux-x86_64/xpi

for _lang in "${_languages[@]}"; do
  _locale=${_lang%% *}
  _pkgname=thunderbird-i18n-${_locale,,}

  pkgname+=($_pkgname)
  source+=("thunderbird-i18n-$pkgver-$_locale.xpi::$_url/$_locale.xpi")
  eval "package_$_pkgname() {
    _package_i18n $_lang
  }"
done

# Don't extract languages
noextract=()
for _src in "${source[@]%%::*}"; do
    case "$_src" in 
      *.xpi) noextract+=("$_src") ;;
    esac
done

sha512sums=('e2ce59eefb0c4df3eb20af01af2b7ad78a09e0fbac7bcc8800538d6655ca63a5d132c0700e2465654cc707a50aee01c62df0532f2c53b5f11c2d3a7ca881d8f0'
            'SKIP'
            'a0061fcb2a7f66061e336a8d95948592f56f4752e56467f14ba63846720ebf845cce7511d1a2637e3b80d5a1ffdaa2fb783fa37195103425ef65222d45372012'
            '6918c0de63deeddc6f53b9ba331390556c12e0d649cf54587dfaabb98b32d6a597b63cf02809c7c58b15501720455a724d527375a8fb9d757ccca57460320734'
            '5cd3ac4c94ef6dcce72fba02bc18b771a2f67906ff795e0e3d71ce7db6d8a41165bd5443908470915bdbdb98dddd9cf3f837c4ba3a36413f55ec570e6efdbb9f'
            '8c855cac4ca59b01b31cd7926bab99c43cdc4fb0d573571213708a2de1658ec00b1bc1f524ed01308de9d15ba0390ba8df8928aee9a77bcae07d93ad0869bd6e'
            '7e43b1f25827ddae615ad43fc1e11c6ba439d6c2049477dfe60e00188a70c0a76160c59a97cc01d1fd99c476f261c7cecb57628b5be48874be7cf991c22db290'
            '3526402ccae1f0428f2e45bae8d0b2cb909ac2698bc3508b692b827839ccb21203ce414206039776f6ce946fc53e636290b7870e9886284d5e9d1e8ad050aac9'
            '36d9662fc94cbf7dcf371adc13a9cda679bc75df961d86de019d3c8ebb0be3062d5ef762d175fab58696db74758100a65de45d7832e0e2bd4e15c901f72d8349'
            '960d9abcb20ce01ad1156c0851e8911f20de7de6e81968f85eeffadec76a1e322c7072b098f8001f0f9f252ccd5079efbec1d4ca41bcd2c5202982e3d7590b99'
            '3ad66f30160bb1ec4d881849e96e511cfd71cda4b1b5d559bcec5e4268085e2c6fad30b7a7ce276b9e01cc70193bdc097e0870538c60ff3c4d86e8e23735b9b4'
            'f07d7aa6d5c809506a10ddd79d1542045f4d222eca7ec5889a06ce539e10a291b8fae89cb500476a8945c2d2a093e102e08fe56cb27e9211ddd2780805a429f6'
            '2c943a0dc0fccc9aa93e710adf03425097635b8753152f6e7b2d4a76defcfd79a5c88ff1444f3b6ca4ef0b9ab13095515e09abd629deaa6030a08cdc73269cf9'
            '0cd4ea1726d7dda209e0bb122aae68544e4fdb5c0b88abbf888349288e50aeb517019123f0a591fb8e8b3c84fc82200d5998c2c5f64865a1ac113a25bf5979f1'
            '3b722edaec0b55638a710423e3063b9ac9e16afb8789da63dcddfe07aa2457778c322d734c8a5fb4f657465d5db488bdf0c2556bf5ea4c91a11f508ad300c8f3'
            '1f8ba1f27f5e598ffb2d397b37f043cb50b345daa0cb83831627c2db8ca22aec20035065b34e8d13b6bc3e8c02ed9632c7aa233c62f59ed4e56b7546a2d75893'
            '436830dad34a76a4c002756e409003dce5bff76db10e3909935b0577b57699268f000be889c77e9bef0471dd22c33b3a5b25a4cfcff4e83089e076b890a0b55c'
            '0afc09e8a0c3f835c66e2998b72e7f26140bc19ea26c59ee0586004d6625edc59d4bf61033d8f162b0f54134188664ab7cf480f800d5f17a776f377f1c0447d5'
            'ebb0ab6a308f384461bc316c82464d0fe638bd274030a7d80fb591b78b5981412ca3d9fd2328fd9c843df0c5c86a1faae4cf766e72767f7effbcc29470c1322c'
            'f414320caed38831bcd4a5149181bfae88bf60501cda40ea70d223566d68621e24f328d606a739a5dc7b95fadff8c61ec18dc145ab6759a6d3648c1adde08172'
            'e3655ab5b0f27226aec1b327611930fdaa1e27c8b7756ff8e38e386513555c8f06a6bbc546d5dcba905265c85f9dce8f7d6310088b13bc6aeecd67e090dd8a06'
            'ff5b38c4e85421f1014fb840d4d39c53be8c739f10c27763911273456ad99f56a69d74da31a212b6b1cf4cc91c66cab465516eaf223101c58f73e00d9dab5e53'
            '86a58f020dffdda717d8f35014a39fc7cbcf9b4e53ab8e1c0bc1517deabd5cf436df3802cd41f0d414a485f037cbf45c4442621f89848c07c5c9a3d1e2a90eb3'
            'bef6622b2db1af15f04af5ed911b02009aafcfb24702de0f9af2a99d7ac13e59c34836e89c87b48c84caa75a624c7edbce797620eac0e4200b58e95b68e48999'
            '2b6a77603a5c7aa1dc04f73592fddadbb874ec802b2eadcc753f0af8a9112c08412875f6b5a563a5269803387c654b3cdb5d86d263ce0ec5246ac7fb59226a91'
            'a6c23aedc066db57a86a078f3ec4fafea6270603b29d510ed4afa72440b27b5dcfae8528c00d490350a3dd711475de55d9e537ca56b28cae47de4605fc941178'
            '1a9aa8202a58d85aa9888e2fdc67f16dd5fa9c2c56335059009cc530dbe8cee6cd5387cdc569091ffc2038f90ca6a813eb8215329c9770acddc256aae12c6b80'
            '98b72ae0e3c1910180ce5a92baf5d3e6818807467d5348922ef0bff88d25e53d1099360a20322d11a865d1a07c0e3ac422d7903cfc48be045e0cc41906ac3115'
            '8bee9d6e4fdd4ec30e8cc433b2f5a753357009b8fd820cbfc11db26bbb7e9e417cf8057d47a703db64c9cc70081a0f0548eb4caa218d6eda45adcd912eb093f4'
            '2ab10a6770fa76027c7addd25dc932ba7d2e0e2c7cf268c9703f6303d4f8de3dcc2375924c46f22ab814f3444075da373886d5a353abec3b5f91ee9678cf8d70'
            '04353cfb6280203927a04e18b0ae8b4537dc4043226df9676b7a172501b8cb9beb74689bc76ec4cf7ddf7faf6519e08151e80a80e15a824f38fef2f43353307a'
            '0ce4fc06930f35079c01bff7e96a590c2eed522baf0502f2a26626449580230762bcd8ac59ae4f8397bdc00e7ca21e970a88b1abf2792ab76faa71fcf91d6a74'
            '34e7ef17fef7fc441b80c77b0ee6fcdc0199b3037b4441d9a4aad62e0a6779fcefc698d00e6d1e2dde45aef2af9a414362d2427cb6107f3d7cb39f5735948301'
            '5c7404181ed596af4bb7145d8fabbecef56de1182918add6a9040cb2c80a5da7b1ec4021d5fecec6f81d0bb4c56ce9a37f259d424d3b5ddf577f528e228fb676'
            '55e0df21fc49598c78be1505fec61c13df4e7cc4d0c79af73685ca2cab4e629c0cb27bc440303319aa412252e41589726f4a454cfbe44e3701b817ceec3f704e'
            'a1f6a6c2a6f3527f1d89fec8e3afea4fb726068d2aa48213121dcfb9ee44edbab90e81d2b600dbe4eabb331aa15ff8d55ddc317b608cf593859d7b39cab38447'
            '950c424ff0a0b9674f99328826f6b6388eb1ebe36b2ecf4f74da6399453136f654315a03167de2dee69c573122ba20e1a723ed465300de3465ac82748543164a'
            '87efbec22e2a64717ce1f1e78ec50ca397975bdcf1a30a63e8a0831652435632aa5385add2b424d696a37c316607baadd2bfe8e4f60f62057bd47667838cc660'
            'fd13f1ee33e15fbb0a82c423b734f6411e2d83eaae2981e2fa1609eb0764b3badd44ddca3b9258346185eed372c17552d82ffe6f5d104ef4905366f5d2bfb306'
            '397a51d8002bedf663f3a93e091bbd40b596f005e8eb4579ddeddb57eb9b665917e3e1aae11c9cb4d6a99563eedb41c0c9474fe32701e442b34afb2a5a594787'
            '71e5af5304d9f611282a9d6a3d4d53522ce8111ae870b480ad0a52bc0203c3c0f7e65aea94984528fbcf2f0c63f0b749c8beebc842bceb8e14b972cc898356bd'
            'd9f8546136559176bd60e240f1adbe1f7429c43a84cf072b07ba5e33dc96a411dd904eac7ac5e889c78b1e144d17747def0c4936b383e557eec3be9d0ccee2ec'
            'e0e8dc5e71f9e1caedb24e6f2a142444ef00f8315c896a45cfe3e8cbb1635729367a5f9dbfb745381428fb66ca8b5791ef63f759e4d31ae3e1fd1dbb9811a360'
            '9a719c8f56939bdf00bde16388141df122460b33ae723f25e383f05a9cc916babb3b7c015a44d7c52f2dd85d6c49a43fdb12bdac4ccbbb5c92c2875c147a3120'
            '8fd9a4da911071ccd4dc1cbffa70fe0258651fe1f8c1d3484c562f62ed9e47a2e13a282212e14303a5ae5e2e1457e18b1ddc71847ca90ccbf7af6acb9031da1f'
            'ab72ee2fd53214c4f12f90beb7d89a2d31385ef5789a38382356c2767aa821c75568f71a4cd507e71cf324ae223e0e1a90127aa3d3481bfaee8ce2271c894469'
            '409269a687e80a9a772c770dffa961aaf792dcdac290a83c7dd90fbadc55419ba64fe7db08246fc36b72a498ccb8e8eebd32e39c51eb88ff19fd2071b0eb4c3a'
            'ef92beb1f78e0badd38056851e202d228f3712be4db759791328832050b218fe2550af611ba2fc418607dc7d67729e8f873edafdd22af903bc789063ee504185'
            '81f6b8d1006aa8274ea10559eff434986bc41cbba52d3d224bbfee86872ad67f69c38c7181d6886738a2a59d31a3ab7c20f476a2269165dc43098120ba2e9a36'
            '2771825fb0d704c5d0c581b700ba7daad33133414e415320d229ffb7542f8e009ba36cee9c93bfb02fda6700dccb2d160ba6934396efcda8b317552692be43e3'
            'b136d95f3cbc231f18312dea62da761fd39cedafc3e3cb6c3127474afb789cd72ec24676c7a41a745655ba1e7621fd0575934822569bb4f5d6f008e5212f7a5d'
            '2f948d62c9af24233e84f61ebdd55267a58eb43ce9820d1e78b5052dc5ddec3d2f58cbd9c1b47197e13925acdad6a3fd935b7a09a0a5a85498489c15a75d6dd0'
            '2a815dd05bf8bb92e8b1cabc2b7de876d406f1d5557d236a3e62005db020732492cb4f962ac3f89f2929027ff8c45f4888250d902ec257332dfa43a9477a892d'
            'a8da02241ce52892b90f823f4ab324922537543d29b681a870dc38aab3c25e11a790710f2847d6e70c52cd322a599b9b4eb39909f96c690917d41db408c244ca'
            'eb542db2a3ff02c8ce427adcf0ef886969bcb5d62503fbb51f4d90bd522d384c2158176a37c1f92bdef1edaaf243b2e88f868e06483bc9b22cd2f9d831545adf'
            'ef3924bb386ac3171636f3d7119fd0843279466e74c9e99de0dc9447b928f3aac99f8893d4040a345cdb252d46533a8c2304fb3596f10d12eccf9f98904034d3'
            '367492142cad43ff400b20d610978b36b75048a782f1acb21f7b624f32d582fab9d82411789c0e552217c76537ee960474e21eeb12547d3010ef39d4682fce3b'
            '8a060e845079ff0f790f9f2b00c49c9cc2ad21e7b5990c3e471eef4d85d0210c2328b13fec63265ef29cab44817f960c526b1623a8a4490baa71b48d38e337cc'
            '24951e059d9a70d89bb60e981317bd51effa25002269c1da3650b83dba568217fb8dc3b0d887450d1f653dab948d324b350176d96c2f01ed398ecb74d8969cc7'
            '76b6b42c1dadfab6826bcbe3f99372fe1a099fa7ed4a2ffb73086c2b03e60092e01a683a20422bfcc4c95da00a804df80f8c122b8409e873f103c448d223fa32'
            '05a20a3379a352503bfacc23a45db79d41c1db4c98dbd7e3ae5caf17fe948e153e8a8b1501525bc73bb73113beaada160bc942f160bbb67fd65ea7ebc38c0aee'
            'a93b99d47cb46739e6ea619a621a6cb34979cbee5437bbdb368646c73e2b7690564748fd2c596f5747d78b6799f257ba5b5bf55a76fc0660034b6335159e44c6'
            '44ec0002fd48a59b85946b4742552a46fb75893d98c5377f2c8c167c9200619c83e08db07d2a436aa54fbdc2b4dbeacafa5e65dc6470b95c52416653f2e58f5c'
            'f42c6f145fd4119cc1a3540a0b088c1374e1ee98fb43462b7503f1c39618892e8f0825dd3b8f7dffeaaca62e7f306b819a0146e865c5d4d5c25793456c01e10e'
            'c78f4bcd872d366884f7fffb39fca862a3bca86f5e908f680837128dfd6368807a47e724910b0cffc4bf6d887e161b4fdf84bad2ecd5f564f92f17020e0bee12'
            '718c8494afbde390ce89a692c63888d426dc8d2873e15eba8162b2dcad6d78fb2d84fc3291fb123f02c0ae7cae2aea1bad6ce897d634bdb7bca61d7e72ef473d'
            '1bb77e85741d6b56dab07287241760ef23b26d6b98eff09e1de70206524420eca2933e73416e22af5a7e7c0afb3daf0e4e98286fc5f550f37398f6ed2a5390aa'
            '49953e283b30810306510a5221becf32f66d9cac1772b087052d99a8602fd56b1b3217ef776ab93f96fbfcce8a85488949492d074b51e0244c090158e7fb879d'
            'e7ecca1e673715d333bd6a9091fee2122db8cacb43c2f12852b576f3f75726f54555f833bf93ae2cec3b4c7a9e281ee01b558e45cba6e08f5ede9415991a0188'
            '64ef76c95df421dfbc47df316d4a6981443c874ad71ca0bc427d222f4114c8a10d747c0fe4a51bf7cd3f03c9d4e7bed2d10e059587c70e67a0b473575f9f0904'
            '93dc589af87444651e29c1208d4c3e301b0eb73fb03d2ae927620c5c3f36763695e28a5cf7fc15b88a111148a8b86a47de8002955cfa32e62af60c9beff4a45a'
            'de6327c2a9059a89768c72ada5f62d6d1dbc5b48c824702aa0ed8d4ef29797a4f5d2d1045dfe0973316190330ebefd512b791d01d3c67c591f994df053fcb944')

# vim:set sw=2 et:

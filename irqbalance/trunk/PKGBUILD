# Maintainer: David Runge <dvzrv@archlinux.org>
# Contributor: Dan McGee <dan@archlinux.org>
# Contributor: Martin Striz <ms@poruba.net>

pkgname=irqbalance
pkgver=1.9.1
pkgrel=1
pkgdesc="IRQ balancing daemon for SMP systems"
arch=(x86_64)
url="https://github.com/irqbalance/irqbalance"
license=(GPL2)
backup=(etc/$pkgname.env)
depends=(glibc libnl)
makedepends=(glib2 libcap-ng ncurses numactl systemd systemd-libs)
options=(debug)
source=($pkgname-$pkgver.tar.gz::https://github.com/$pkgname/$pkgname/archive/v$pkgver.tar.gz)
sha512sums=('ec2abd3aad61e5370ca13a767fb6b5b206b61f5751853995780dd62e1657d88d74819ff5838ad2599855c701ea5d53755bf108a5427469faa7b1f042351b6068')
b2sums=('218e6f90d0e9ca9b2e276a69fd0c97438ff16c96772f30281c10a051eafdf929a66f13c03d512232fff836aa1c2095bc404006dfc16904c2c2cba245f4b57b59')

prepare() {
  # fix location of configuration and binary in service
  sed -e 's|/path/to/|/etc/|g;s|/usr/sbin|/usr/bin|g' -i $pkgname-$pkgver/misc/$pkgname.service

  cd $pkgname-$pkgver
  autoreconf -fiv
}

build() {
  cd $pkgname-$pkgver
  ./configure \
    --prefix=/usr \
    --sbindir=/usr/bin \
    --with-systemd
  make
}

check() {
  make -k check -C $pkgname-$pkgver
}

package() {
  depends+=(libglib-2.0.so libcap-ng.so libncursesw.so libnuma.so libsystemd.so)

  cd $pkgname-$pkgver
  make install DESTDIR="$pkgdir"
  install -vDm 644 misc/irqbalance.service -t "$pkgdir/usr/lib/systemd/system/"
  install -vDm 644 misc/irqbalance.env -t "$pkgdir/etc/"
  install -vDm 644 {AUTHORS,README.md} -t "$pkgdir/usr/share/doc/$pkgname/"
}
